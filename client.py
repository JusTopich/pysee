from socket import socket, AF_INET, SOCK_DGRAM
import sys, signal, time

try:
    import cv2
except:
    print('install cv2 type: "pip install opencv-contrib-python"')    
try:
    from numpy import frombuffer
except:
    print('install numpy type: "pip install numpy"')



# host = '10.10.18.31'
host = 'localhost'
port = 777
addr = (host,port)
udp_socket = socket(AF_INET, SOCK_DGRAM)
udp_socket.settimeout(None)
# udp_socket.settimeout(1)

class SIGINT_handler():
    def __init__(self):
        self.SIGINT = False

    def signal_handler(self, signal, frame):
        print('You pressed Ctrl+C!')
        self.SIGINT = True
        sys.exit(1)


handler = SIGINT_handler()
signal.signal(signal.SIGINT, handler.signal_handler)

def show():
    # data = udp_socket.recvfrom(1024)[0]
    raw = b''
    st = time.time()
    while True:
        try:
            data = udp_socket.recvfrom(1024)[0]
            if data[:5]==b'shape':
                shape=int(data[5:])
                continue
            if data==b'frame':
                img = frombuffer(raw, dtype='uint8').reshape((shape,1,))
                decimg = cv2.imdecode(img, 1)
                cv2.imshow('OpenCV/Numpy normal', decimg)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    cv2.destroyAllWindows()
                    break
                raw=b''
                print(f'\rfps: {1 / (time.time()-st)}',end='')
                st = time.time()
            elif data == b'stop':
                break
            else:
                raw=raw+data
        except Exception as e:
            # print(e)
            raw=b''
    print('\nstop show')
    cv2.destroyAllWindows()

while True:
    if handler.SIGINT:
            break
    
    print('show cmd: show scale[0.1-1] packetsize[1-64] quality[1-100] frames[1-inf]')
    send = input('write to server: ')
    # send='show 1 40 1 100'
    if send=='exit': 
        break

    #encode - перекодирует введенные данные в байты, decode - обратно
    send = str.encode(send)
    udp_socket.sendto(send, addr)
    try:
        data = udp_socket.recvfrom(1024)[0]
        if data==b'showing':
            show()
        else:
            print(data)
    except Exception as e:
        # print(e)
        pass

udp_socket.close()
sys.exit(1)
