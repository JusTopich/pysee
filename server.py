from socket import socket,AF_INET,SOCK_DGRAM
import signal,sys,time
try:
    import mss
except:
    print('install mss type on win10: "pip install --upgrade mss"') 
try:
    import cv2
except:
    print('install cv2 type: "pip install opencv-contrib-python"')    
try:
    import numpy
except:
    print('install numpy type: "pip install numpy"')

#данные сервера
# host = '10.10.18.31'
host = 'localhost'
port = 777
addr = (host,port)

#socket - функция создания сокета
#первый параметр socket_family может быть AF_INET или AF_UNIX
#второй параметр socket_type может быть SOCK_STREAM(для TCP) или SOCK_DGRAM(для UDP)
udp_socket = socket(AF_INET, SOCK_DGRAM)
udp_socket.settimeout(None)
udp_socket.bind(addr)

class SIGINT_handler():
    def __init__(self):
        self.SIGINT = False

    def signal_handler(self, signal, frame):
        print('You pressed Ctrl+C!')
        self.SIGINT = True
        udp_socket.close()
        sys.exit(1)


handler = SIGINT_handler()
signal.signal(signal.SIGINT, handler.signal_handler)

print('wait data...')
while True:
    if handler.SIGINT:
            break
    try:
        query=udp_socket.recvfrom(1024)
        data=query[0]
        addr=query[1]
        print(addr[0],data)
        if data[:4]!=b'show':
            try:
                udp_socket.sendto(b"I'm alive!", addr)
            except:
                pass
        else:
            data=data.decode('ascii').split(" ")
            try:
                scale=float(data[1])
                # print('!#scale',scale)
                size=int(data[2])
                # print('!#size',size)
                quality=int(data[3])
                frames=int(data[4])
            except:
                scale=0.5
                size=30
                quality=25
                frames=100
            try:
                udp_socket.sendto(b'showing', addr)
                with mss.mss() as sct:
                    # monitor = {'top': 200, 'left': 150, 'width': 600, 'height': 340}
                    # monitor = {'top': 200, 'left': 600, 'width': 800, 'height': 450}
                    monitor = {'top': 100, 'left': 100, 'width': 1280, 'height': 800}
                    # monitor = sct.monitors[1]
                    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), quality]
                    size=40

                    n=0
                    while n<frames:
                        img = numpy.array(sct.grab(monitor))
                        img=cv2.resize(img,(0,0), fx=scale, fy=scale)
                        result, encimg = cv2.imencode('.jpg', img, encode_param)
                        encimgB = encimg.tobytes()
                        shapeB=str(len(encimgB)).encode('ascii')
                        # st=0
                        # nd=40
                        # while encimgB[st:nd]!=b'':
                            # udp_socket.sendto(encimgB[st:nd], addr)
                            # st+=size
                            # nd+=size
                        # udp_socket.sendto(b'frame', addr)
                        # n+=1

                        udp_socket.sendto(b'shape'+shapeB, addr)
                        while encimgB!=b'':
                            udp_socket.sendto(encimgB[:size], addr)
                            try:
                                encimgB=encimgB[size:]
                            except:
                                udp_socket.sendto(encimgB,addr)
                                encimgB=b''
                        udp_socket.sendto(b'frame', addr)
                        n+=1

                    udp_socket.sendto(b'stop', addr)
                    # udp_socket.sendto(b'stop', addr)
                print('stop show')
            except Exception as e:
                print(e)
    except:
        pass

